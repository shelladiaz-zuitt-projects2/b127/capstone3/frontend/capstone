//Bootstrap
import Carousel from 'react-bootstrap/Carousel';
import { Card } from 'react-bootstrap';

export default function Banner(){

	return(
			<Card.Body>
					<Carousel>
					<Carousel.Item interval={1000}>
						<img
						className="d-block w-100"
						src="https://ppe.stylistinpocket.com/admin/assets/stores/WebBannerC5-0923200750465f6afe56c8f1c.jpg"
						alt="First slide"

						/>
						
					</Carousel.Item>
					<Carousel.Item interval={500}>
						<img
						className="d-block w-100"
						src="https://ppe.stylistinpocket.com/admin/assets/stores/WebBannerC5-0923200750465f6afe56c8f1c.jpg"
						alt="Second slide"
						/>
						
					</Carousel.Item>
					<Carousel.Item>
						<img
						className="d-block w-100"
						src="https://ppe.stylistinpocket.com/admin/assets/stores/WebBannerC5-0923200750465f6afe56c8f1c.jpg"
						alt="Third slide"
						/>
						
					</Carousel.Item>
				</Carousel>
			</Card.Body>
		)
}

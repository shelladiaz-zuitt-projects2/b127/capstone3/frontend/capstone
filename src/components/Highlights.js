import {Row, Col, Card, Button} from 'react-bootstrap'
import { Fragment } from 'react'


export default function Highlights(){
   return(
    <Fragment>
        <h1 className="copper-text text-center pb-2">
            <u>Upcoming Sale!</u>
        </h1>
        <Row>
            <Col xs={12} md={4}>
                <Card className="cardHighlight  p-3 copper" text="white">
                    <Card.Body>
                        <Card.Title><h2>11.11</h2></Card.Title>
                        <Card.Text>
                           Watch out our upcoming 30% sale this coming November 11, 2021. More bags we're gonna be on sale.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>

            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3 copper" text="white">
                    <Card.Body>
                        <Card.Title><h2>Christmas Sale</h2></Card.Title>
                        <Card.Text>
                           From Decemeber 6-17, watch out our biggest sale of the year! 70% less on most of our items. 
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>

            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3 copper" text="white">
                    <Card.Body>
                        <Card.Title><h2>Garage Sale</h2></Card.Title>
                        <Card.Text>
                          New year, New Bag! Watch out our Garage Sale on January 1-30, 2021. Check for more sale items.                        
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>

    </Fragment>
    
   ) 
}
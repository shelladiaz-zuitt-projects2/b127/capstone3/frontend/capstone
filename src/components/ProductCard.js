import { useState, useEffect } from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export default function ProductCard({productProp}){
    
    const { _id, name, price } = productProp;

    
    return(
     <Row>
         <Col>
             <Card className="productCard p-1 mt-3 text-center">
                <Card.Body>
                    <Card.Title><h4>{name}</h4></Card.Title>
                    <Card.Text><h5>Php {price}</h5></Card.Text>

                     <Link className="btn copper text-white" to={`/products/${_id}`}>Details</Link>
                     
                     
                </Card.Body>
             </Card>
         </Col>
    </Row>
    )

 }



ProductCard.propTypes = {
    
    productProp: PropTypes.shape({
        
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}
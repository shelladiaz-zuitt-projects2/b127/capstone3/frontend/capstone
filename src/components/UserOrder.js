import {Fragment, useEffect} from 'react';
import {Table} from 'react-bootstrap';
import {Link} from 'react-router-dom'


export default function UserOrder({orderData}){
	const {name, quantity, totalAmount, orderOn, _id} = orderData;

	return(
		<Fragment>
		<h1  className="copper-text text-center">Transaction History</h1>
			<Table striped hover>
				<thead className="bg-dark text-light">
					<tr className="text-center">
						<td className="w-25">Date</td>
						<td className="w-25">Name</td>
						<td className="w-25">Quantity</td>
						<td className="w-25">Total</td>
					</tr>
				</thead>
				<tbody className='hover'>
					<tr key={_id} className='text-center'>
						<td className="w-25">{orderOn.slice(0,10)}</td>
						<td className="w-25">{name}</td>
						<td className="w-25">{quantity}</td>
						<td className="w-25">{totalAmount}</td>
					</tr>
				</tbody>
			</Table>
		</Fragment>
	)
}

import { useState, useEffect, Fragment } from 'react';
import ProductCard from '../components/ProductCard';


export default function UserView({ productData }){
    const [products, setProducts] = useState([]);
    
    useEffect(() => {
        const productsArr = productData.map(product => {
            if(product.isActive === true){
                return(
                    <ProductCard productProp={product} key={product._id}/>
                )
            }else{
                return null;
            }
        })
        setProducts(productsArr)
    }, [productData])

    return (
        <Fragment>
            {products}
        </Fragment>       
    )
}
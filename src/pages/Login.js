import { useState, useEffect, Fragment, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
//React Context
import UserContext from '../UserContext';
//routing
import { Link, Redirect, useHistory } from 'react-router-dom';

export default function Login(){
	const history = useHistory();

	const { user, setUser } = useContext(UserContext)

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [loginButton, setLoginButton] = useState(false)


	useEffect(() =>{
		if(email !== '' && password !== ''){
			setLoginButton(true)
		}else{
			setLoginButton(false)
		}
	}, [email, password])


	function login(e){
		e.preventDefault();

		fetch('https://pure-plateau-66098.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken);
				setUser({ accessToken: data.accessToken });

				Swal.fire({
					title: 'Yaaaaaaay!',
					icon: 'success',
					text: 'Thank you for loggin in to Personal Ecommerce'
				})

				//get user's details from our token
				fetch('https://pure-plateau-66098.herokuapp.com/users/mydetails', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if(data.isAdmin === true){
						localStorage.setItem('email', data.email)
						localStorage.setItem('isAdmin', data.isAdmin)
						setUser({
							email: data.email,
							isAdmin: data.isAdmin
						})
						//If admin, redirect the page to /courses
						//push(path) - pushes a new entry onto the history stack
						history.push('/products')
					}else{
						//If not admin, redirect to homepage
						history.push('/')
					}
				})

			}else{
				Swal.fire({
					title: 'OOoooops!',
					icon: 'error',
					text: 'Something Went Wrong. Check your Credentials.'
				})
			}
			setEmail('')
			setPassword('')

		})
	}

	//Create a conditional statement that will redirect the user to the homepage when a user is logged in.
	return(
		(user.accessToken !== null) ?
			<Redirect to="/"/>
			:
	<Fragment>
		<h1 className="copper-text">Login</h1>
		<Form onSubmit={e => login(e)}>
			<Form.Group>
				<Form.Label className="copper-text">Email Address:</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter you email here"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label className="copper-text">Password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter you password here"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>

			{loginButton ?
			<Button className="copper text-white" type="submit">Submit</Button>
			:
			<Button className="copper text-white" type="submit" disabled>Submit</Button>
			}

		</Form>
		<h6 className="pt-2">
			
		<Link to="/register">Create an account?</Link>
		
		</h6>
	</Fragment>

		)
}
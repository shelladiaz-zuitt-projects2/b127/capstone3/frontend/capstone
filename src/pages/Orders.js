import {useState, useEffect, useContext, Fragment} from 'react';
import {Link} from 'react-router-dom'
import UserContext from '../UserContext'
import UserOrder from '../components/UserOrder';

export default function Cart () {
	const {user} =useContext(UserContext);
	const [orders, setOrders] = useState([]);

	const fetchOrder = () =>{
		fetch(`https://pure-plateau-66098.herokuapp.com/users/view`, {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res=>res.json())
		.then(data=> {
			if (data.length !== 0) {
				const orderArr = data.map(order=>{
					return <UserOrder orderData={order}/>
				})
				setOrders(orderArr);
			} else {
				setOrders();
			}
		})
		
	}

	useEffect(()=> {
		fetchOrder()
	},[])

	return(
		user.isAdmin?
		<div className='text-center copper-text pt-5'>
			<h4>Soon will be available</h4>
		</div>
		:
		orders==null?
		<div className='text-center copper-text pt-5'>
			<h4>No Orders Yet. Click <Link to='/products'>here</Link> to view our products.</h4>
		</div>
		:
		<Fragment>
			{orders}
		</Fragment>
	)
}

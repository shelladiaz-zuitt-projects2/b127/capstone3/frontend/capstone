import {useState, useEffect, useContext} from 'react';
import {Container} from 'react-bootstrap';

import UserView from '../components/UserView';
import AdminView from '../components/AdminView';
import UserContext from '../UserContext';


export default function Products(){

    const { user } = useContext(UserContext);

    const[allProducts, setAllProducts] = useState([]);

    const fetchData = () => {
        fetch(`https://pure-plateau-66098.herokuapp.com/products/all`)
        .then(res => res.json())
        .then(data => {
            console.log(data)
            setAllProducts(data)
        })
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <Container>
            {
				(user.isAdmin === true) ?
				<AdminView productData={allProducts} fetchData={ fetchData }/>
				:
                <UserView productData={allProducts}/>      
			}      
        </Container>
        
    )
}
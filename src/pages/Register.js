import { Fragment, useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import {  Link, Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';


export default function Register(){
	const { user } = useContext(UserContext);

	const history = useHistory();

	//State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [address, setAddress] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false);

	//Check if values are successfully binded
	console.log(email)
	console.log(password1)
	console.log(password2)

	useEffect(()=>{
		//Validation to enable submit button when all fields are populated and both passwords match
		if((firstName !== '' && lastName !== '' && email !== '' && mobileNo.length === 11 && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}

	}, [firstName, lastName, email, mobileNo, address, password1, password2])


	function registerUser(e){
		e.preventDefault();
		
		//check the email duplicates
		fetch('https://pure-plateau-66098.herokuapp.com/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: 'Duplicate email Found',
					icon: 'error',
					text: 'Kindly provide another email to complete the registration.'
				})
			}else{
				fetch('https://pure-plateau-66098.herokuapp.com/users/register', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						address: address,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)

					if(data === true){
						Swal.fire({
							title: 'Registration successful!',
							icon: 'success',
							text: 'Welcome to Shop!'
						});

						history.push('/login')
					}else{
						Swal.fire({
							title: 'Something Went Wrong!',
							icon: 'error',
							text: 'Please Try Again!'
						});
					}

				})


			}

		})

		setFirstName('')
		setLastName('')
		setEmail('')
		setMobileNo('')
		setAddress('')
		setPassword1('')
		setPassword2('')
		
	}


	return(
		(user.accessToken !== null) ?
		<Redirect to="/"/>
		:
		<Fragment>
			<h1 className="copper-text">Register</h1>
			<Form onSubmit={(e) => registerUser(e)}>
				<Form.Group>
					<Form.Label className="copper-text">First Name:</Form.Label>
					<Form.Control
						type="text" 
						placeholder="Enter your First Name"
						value={firstName}
						onChange={e => setFirstName(e.target.value)}// the e.target.value property allows us to gain access to the input field's current value to be used when submitting form data
						required
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label  className="copper-text">Last Name:</Form.Label>
					<Form.Control
						type="text" 
						placeholder="Enter your Last Name"
						value={lastName}
						onChange={e => setLastName(e.target.value)}// the e.target.value property allows us to gain access to the input field's current value to be used when submitting form data
						required
					/>
				</Form.Group>

				<Form.Group>
					<Form.Label  className="copper-text">Email address:</Form.Label>
					<Form.Control
						type="email" 
						placeholder="Enter email"
						value={email}
						onChange={e => setEmail(e.target.value)}// the e.target.value property allows us to gain access to the input field's current value to be used when submitting form data
						required
					/>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>


				<Form.Group>
					<Form.Label  className="copper-text">Mobile Number:
					</Form.Label>
					
					<Form.Control
						type="text" 
						placeholder="Enter your Mobile Number"
						value={mobileNo}
						onChange={e => setMobileNo(e.target.value)}// the e.target.value property allows us to gain access to the input field's current value to be used when submitting form data
						required
					/>
					<Form.Text className="text-muted">
						Make sure your number is 11 digits.
					</Form.Text>
				</Form.Group>

				<Form.Group>
					<Form.Label  className="copper-text">Address:</Form.Label>
					<Form.Control
						type="text" 
						placeholder="Enter your Address"
						value={address}
						onChange={e => setAddress(e.target.value)}// the e.target.value property allows us to gain access to the input field's current value to be used when submitting form data
						required
					/>
				</Form.Group>




				<Form.Group>
					<Form.Label  className="copper-text">Password</Form.Label>
					<Form.Control 
						type="password"
						placeholder="Enter your Password"
						onChange={e => setPassword1(e.target.value)}
						value={password1}
						required
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label  className="copper-text">Verify Password</Form.Label>
					<Form.Control 
						type="password"
						placeholder="Verify your Password"
						value={password2}
						onChange={e => setPassword2(e.target.value)}
						required
					/>
				</Form.Group>
				{isActive ?
				<Button variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
				:
				<Button variant="primary" type="submit" id="submitBtn" disabled>
					Submit
				</Button>
				}
			</Form>
			<h6 className="pt-2">
			
				Already have an account? <Link to="/login">Login</Link>
			
			</h6>

		</Fragment>



		)
}
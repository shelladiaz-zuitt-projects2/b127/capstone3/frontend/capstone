import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Form, InputGroup } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Link, useHistory, useParams } from 'react-router-dom';

import UserContext from '../UserContext';


export default function SpecificProduct(){

	const { user } = useContext(UserContext);

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState(0)
	const [qty, setQty] = useState(1);

	const { productId } = useParams();
	const history = useHistory();

	const addQty = e => {
		if (qty<99) {
			setQty(qty+1)
		}
	}

	const subQty = e => {
		if (qty>=1) {
			setQty(qty-1)
		}
	}
	



	useEffect(() =>{
		fetch(`https://pure-plateau-66098.herokuapp.com/products/${ productId }`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, [])


	const checkout = (productId, quantity) =>{
		fetch(`https://pure-plateau-66098.herokuapp.com/orders/checkout`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				Swal.fire({
					title: "Yeeeeeeey!",
					icon: 'success',
					text: `You have successfully purchased for this ${ name } product.`
				})
				history.push('/products')
			}else{
				Swal.fire({
					title: "OOooooops!",
					icon: 'error',
					text: `Something went wrong. Please try again`
				})
			}
		})
	}

	return(
		<Container>
			<Card className="pt-2">
				<Card.Header className="bg-dark text-white pb-0">
					<h4>{name}</h4>
				</Card.Header>

				<Card.Body>
					<Card.Text>
						{description}
					</Card.Text>
					<h6>Php {price}</h6>
					<InputGroup className='w-25 pt-1'>
						<InputGroup.Prepend>
					    	<Button variant="dark" onClick={e=>subQty(e)}>-</Button>
					   	</InputGroup.Prepend>
					   	<Form.Control 
					   		className='text-center w-25' 
					   		value={qty} 
					   		type='text'
					   		maxLength='2' 
					   		onChange={e=>{
					   			if(e.target.value!=='') {
					   				setQty(parseInt(e.target.value))
					   			}
					   			else{
					   				setQty(0)
					   			}
					   		}}
					   	/>
						<InputGroup.Append>
							<Button variant="dark" onClick={e=>addQty(e)}>+</Button>
						</InputGroup.Append>
					</InputGroup>


				</Card.Body>

				<Card.Footer>
					{
						user.accessToken !== null ?
							<Button variant="primary" block onClick={()=>checkout(productId, qty)}>Order</Button>
							:
							<Link className="btn btn-secondary btn-block" to="/login">Login to Order
							</Link>


					}

				</Card.Footer>

			</Card>
		</Container>

		)
}
